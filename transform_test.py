from transform_scale import transform


def test_CeltoKel():
    assert transform(150, 'C', 'K') == 423.15

def test_KeltoCel():
    assert transform(250, 'K', 'C') == -23.149999999999977

def test_CeltoFah():
    assert transform(95, 'C', 'F') == 203

def test_FahtoCel():
    assert transform(75, 'F', 'C') == 23.88888888888889

def test_KeltoFah():
    assert transform(465, 'K', 'F') == 377.33

def test_FahtoKel():
    assert transform(5, 'F', 'K') == 258.15

def test_Error():
    assert transform(1, 'F', 'Kel') == "The scale was entered incorrectly"

